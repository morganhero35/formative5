package formative2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Formative2 {

    public static void main(String[] args) {
        File file;
        Scanner scanner;
        try {
            int i = 0;
            while (i < 10) {
                file = new File("E:\\Bootcamp\\Bootcamp\\formative5\\formative1\\form1\\form"
                        + (i + 1) + ".txt");
                scanner = new Scanner(file);
                while (scanner.hasNextLine()) {
                    String text = scanner.nextLine();
                    System.out.printf("Form " + (i + 1) + ".txt berisi\t: %.20s \n",text);
                }
                scanner.close();
                i++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
