package formative3;

import java.util.regex.Pattern;

public class Formative3 {

    private static Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");

    public static void main(String[] args) {
        String text = "a b c d 1 2 3 4 5 j k l 9 10 11";
        String[] input = text.split(" ");
        int count = 0;
        for (String s : input) {
            if (isNumeric(s)) count += Integer.parseInt(s);
        }
        System.out.println(count);

    }

    public static boolean isNumeric(String strNum) {
        return strNum != null && pattern.matcher(strNum).matches();
    }
}
