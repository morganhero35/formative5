package formative1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Formative1 {
    public static void main(String[] args) {
        String path = "E:\\Bootcamp\\Bootcamp\\formative5\\formative1\\";
        path = path + "form1";
        File file = new File(path);

        boolean addFolder = file.mkdir();
        System.out.println(addFolder ? "Folder form1 berhasil dibuat" : "Folder sudah ada");

        FileWriter isiText;
        int i = 0;
        while (i < 10) {
            file = new File("E:\\Bootcamp\\Bootcamp\\formative5\\formative1\\form1\\form"
                    + (i + 1) + ".txt");
            boolean result;
            try {
                result = file.createNewFile();
                if (result) {
                    System.out.println("Form" + (i + 1) + " berhasil dibuat");
                    isiText = new FileWriter("E:\\Bootcamp\\Bootcamp\\Code\\formative5\\src\\formative1\\form1\\form"
                            + (i + 1) + ".txt");
                    isiText.write("Hello Java " + (i + 1));
                    isiText.close();
                    System.out.println("Form" + (i + 1) + ".txt Terisi");
                } else {
                    System.out.println("File form" + (i + 1) + " sudah ada");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }


            i++;
        }
    }
}
